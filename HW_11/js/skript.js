// 1
/*let first_name = "Dima"
let last_name = "Lukianchenko"
let age = 22
alert (first_name)
alert (last_name)
alert (age)
console.log (first_name)
console.log (last_name)
console.log (age)
*/

// 2
/*const FIRST_NAME = "Dima"
const LAST_NAME = "Lukianchenko"
const AGE = 22
alert (FIRST_NAME)
alert (LAST_NAME)
alert (AGE)
console.log (FIRST_NAME)
console.log (LAST_NAME)
console.log (AGE)
*/

// 3
/*let first_name = prompt ('What is your first name?')
let last_name = prompt ('What is your last name?')
let age = prompt ('How old are you?')
alert (first_name)
alert (last_name)
alert (age)
console.log (first_name)
console.log (last_name)
console.log (age)
*/

// 4
/*const FIRST_NAME = prompt ("What is your first name?")
const LAST_NAME = prompt ("What is your last name?")
const AGE = prompt ("How old are you?")
alert (FIRST_NAME)
alert (LAST_NAME)
alert (AGE)
console.log (FIRST_NAME)
console.log (LAST_NAME)
console.log (AGE)
*/

// 5
/*const AGE = prompt ('Есть ли вам 18 лет?', 18)
alert(`Тебе ${age} лет!`)
*/

// 6
/*let now = Date()
alert(now)
console.log (now)
*/

// 7
/*let now = Date() 
prompt(now)
alert(now)
console.log (now)
*/

// 8
/*let a = prompt ("a = ")
let b = prompt ("b = ")
let c = Number(a) + Number(b)
alert (c)
console.log(c)
*/

// 9
/*
const RED = "#ff0000"
document.body.style.background = RED
*/


// 10
/*
let massColor = ['#ff9933', '#ff99cc', '#0000ff', '#66ff33', '#cccc00']
document.body.style.background = massColor[1]
*/

//11
/*
let objectColor = {
    Red: '#ff0000',
    ff0066: '#ff0066',
    Blue: '#0000ff',
    Lime: '#00ff00',
    Yellow: '#ffff00'
    }
document.body.style.background = objectColor.Yellow
*/

//12

let person = {
    firstName : 'Dima',
    lastName : 'Lukianchenko',
    nikName: 'LD',
    foto: 'https://www.google.com/url?sa=i&url=https%3A%2F%2Fnews.liga.net%2Flifestyle%2Fphoto%2Fsamye-smeshnye-foto-dikoy-prirody-nazvany-40-finalistov-konkursa&psig=AOvVaw1JG-Jm4C2qZieUeVS2mOrv&ust=1620149489447000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKi13rKFrvACFQAAAAAdAAAAABAJ',
    old: '22',
    floor: 'M',
    emil: 'dimaldima5@gmail.com'
}
let res = `${person.firstName} </br> ${person.lastName} </br> ${person.nikName} </br> ${person.foto} </br> ${person.old} </br> ${person.floor} </br> ${person.emil}`
document.getElementById("person").innerHTML = res;


//13
let car = {
        brand: 'brand - ford',
        model: 'model - focus',
        engine: 'engine - V8',
        year: 'year of issue - 2014',
        numberOfValves: 'number of valves - 32',
        volume: 'volume - 2.5',
        comfort: 'comfort - cruise, climate',
        wheDiameter: 'wheel diameter - 15',
        wheWidth: 'wheel width - 195mm',
        loadIndex: 'load index - 91',
        bType: 'body type - sedan',
        trVolume: 'trunk volume - 540l',
        numberOfSeats: 'number of seats - 5',
        weight: 'weight - 1450kg',
        classOfRights: 'class of rights - B'
    }

    
let carInfo = `1. ${car.brand}
</br> 2. ${car.model}
</br> 3. ${car.engine}
</br> 4. ${car.year}
</br> 5. ${car.numberOfValves}
</br> 6. ${car.volume}
</br> 7. ${car.comfort}
</br> 8. ${car.wheDiameter}
</br> 9. ${car.wheWidth}
</br> 10. ${car.loadIndex}
</br> 11. ${car.bType}
</br> 12. ${car.trVolume}
</br> 13. ${car.numberOfSeats}
</br> 14. ${car.weight}
</br> 15. ${car.classOfRights}`

document.getElementById("car").innerHTML = carInfo;

let house = {
    type: 'type - Vacation home',
    region: 'region - Kharkiv',
    area: 'area - 140m',
    numberOfRooms: 'number of rooms - 4',
    yardArea: 'yard area - 16',
    disgraced: 'disgraced - Centralized disgraced',
    numberOfFloors: 'number of floors - 2',
    garage: 'garage - separate garage',
    yeaConstruction: 'year of construction - 2008',
    floorMaterial: 'floor material - laminate',
    wallMaterial: 'wall material - wallblock',
    window: 'window material - plastic',
    waterSupply: 'water supply - central',
    electricity: 'electricity - central',
    neighbors: 'neighbors - peaceful'
}

let houseInfo = `1. ${house.type}
</br> 2. ${house.region}
</br> 3. ${house.area}
</br> 4. ${house.numberOfRooms}
</br> 5. ${house.yardArea}
</br> 6. ${house.disgraced}
</br> 7. ${house.numberOfFloors}
</br> 8. ${house.garage}
</br> 9. ${house.yeaConstruction}
</br> 10. ${house.floorMaterial}
</br> 11. ${house.wallMaterial}
</br> 12. ${house.window}
</br> 13. ${house.waterSupply}
</br> 14. ${house.electricity}
</br> 15. ${house.neighbors}`
document.getElementById("house").innerHTML = houseInfo;
